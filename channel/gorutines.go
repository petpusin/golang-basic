package main

import (
	"fmt"
	"time"
)

func f(from string) {
	for i := 0; i < 100; i++ {
		fmt.Println(from, ":", i)
	}
}

func main() {
	// f("Hello word")
	// f("message3")
	go f("Hello word")
	go f("message3")
	time.Sleep(5 * time.Second)
}
