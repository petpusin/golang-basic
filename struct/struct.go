package main

import "fmt"

type employee struct {
	employeeID   string
	employeeName string
	phone        string
}

func main() {
	// employee1 := employee{
	// 	employeeID:   "101",
	// 	employeeName: "Petch",
	// 	phone:        "09xxxxxx",
	// }

	// fmt.Println("employee1 = ", employee1) //employee1 =  {101 Petch 09xxxxxx}

	//array struct
	// employeeList := [3]employee{}
	// employeeList[0] = employee{
	// 	employeeID: "101",
	// 	employeeName: "Petch",
	// 	phone: "0092-21-023-",
	// }
	// employeeList[1] = employee{
	// 	employeeID: "102",
	// 	employeeName: "Nice",
	// 	phone: "0092-21-023-",
	// }
	// employeeList[2] = employee{
	// 	employeeID: "103",
	// 	employeeName: "Attachai",
	// 	phone: "0092-21-023-",
	// }
	// fmt.Println("employeeList = ",employeeList) //employeeList =  [{101 Petch 0092-21-023-} {102 Nice 0092-21-023-} {103 Attachai 0092-21-023-}]

	//slice
	employeeSlice := []employee{}
	employee1 := employee{
		employeeID:   "101",
		employeeName: "Petch",
		phone:        "09xxxxxx",
	}
	employee2 := employee{
		employeeID:   "102",
		employeeName: "Nice",
		phone:        "0092-21-023-",
	}
	employee3 := employee{
		employeeID:   "103",
		employeeName: "Attachai",
		phone:        "0092-21-023-",
	}
	employeeSlice = append(employeeSlice, employee1, employee2, employee3)

	fmt.Println("employeeSlice = ", employeeSlice) //employeeSlice =  [{101 Petch 09xxxxxx} {102 Nice 0092-21-023-} {103 Attachai 0092-21-023-}]
}
