package main

import "fmt"

// func add(value1, value2 float64) {
// 	result := value1 + value2
// 	fmt.Println("result = ", result)
// }

func loop() {
	fmt.Println("loop")
	for i := 0; i < 10; i++ {
		fmt.Println("i =", i)
	}
}

func deferloop() {
	fmt.Println("defer loop")
	for j := 0; j < 10; j++ {
		defer fmt.Println("j =", j)
	}
}

func main() {
	//default
	// fmt.Println("Welcom to calculator")
	// add(20, 10)
	// fmt.Println("End")

	//using defer (LIFO) last in frist out
	// fmt.Println("Welcom to calculator")

	// defer fmt.Println("End")
	// defer add(20, 10)
	// defer add(61, 25)
	// defer add(14, 15)
	// defer add(12, 12)

	//defer loop
	loop()
	deferloop()
}
